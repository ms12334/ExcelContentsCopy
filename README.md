sample1<br/>
This macro copies specified contents from one excel file to another excel file.
Source excel file and target excel file are specified in the code.<br/>
Refererence: http://www.ozgrid.com/forum/showthread.php?t=180029

sample2<br/>
This macro copies column A to column B and column C to column D within a excel file.
Save this file (xlsm) in the same directory as excel files. Then run this macro.  All excel files in the same directory will be affected.<br/>
Reference: http://www.mrexcel.com/forum/excel-questions/237276-making-same-change-multiple-excel-files.html
